
const CONFIG = require('../../utils/config.js');
const app = getApp()

Page({
  data: {
    motto: '',
    userInfo: {},
    hasUserInfo: false
  },
  //事件处理函数
  bindViewTap: function () {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },
  onLoad: function () {
    var token = wx.getStorageSync('token');
    if (token !== null && token !== '') {
      wx.redirectTo({
        url: '../main/main',
      })
    } 
  },
  goUserXieYi(e){
    console.log('d')
    wx.navigateTo({
      url: '/pages/webview/webview?type=user'
    })
  },
  cancelLogin(e){
    wx.navigateTo({
      url: '/pages/main/main'
    })
  },
  doLogin: function (callback = () => { }) {
    let that = this;
    wx.login({
      success: function (loginRes) {
        if (loginRes) {
          //获取用户信息
          let myapp = that.app;
          wx.getUserInfo({
            withCredentials: true,//非必填  默认为true
            success: function (infoRes) {
              that.setData({
                userInfo: infoRes.userInfo,
                hasUserInfo: true
              })
              getApp().globalData.userInfo = infoRes.userInfo;

              wx.setStorageSync('userInfoNickName', infoRes.userInfo.nickName)

              wx.setStorageSync('userInfoAvatarUrl', infoRes.userInfo.avatarUrl)

              console.log(infoRes, '>>>');
              //请求服务端的登录接口
              wx.request({
                url:CONFIG.API_BASE_URL + "/rest/oauth/wxlogin",
                method: 'post',
                data: {
                  code: loginRes.code,
                  rawData: infoRes.rawData
                },
                success: function (res) {
                  console.log('login success' + res);
                  res = res.data;
                  if (res.code == 0) {
                    //wx.setStorageSync('userInfo', JSON.stringify(res.userInfo));
                    //wx.setStorageSync('loginFlag', res.skey);
                    //console.log("skey=" + res.skey);

                    wx.setStorageSync('token', res.data.access_token)
                    wx.setStorageSync('driver_id', res.data.driver_id)
                    wx.setStorageSync('open_id', res.data.open_id)
                    wx.setStorageSync('driver_phone', res.data.driver_phone)
                    wx.setStorageSync('session_key', res.data.session_key)
                    
                    console.log(res.data.access_token);
                    wx.redirectTo ({
                      url: '../main/main'
                    })
                    // wx.navigateBack({
                    //   delta: 2
                    // })
                  } else {
                    //that.showInfo('res.errmsg');
                  }
                },
                fail: function (error) {
                  //调用服务端登录接口失败
                  // that.showInfo('调用接口失败');
                  console.log(error);
                }
              });
              // wx.redirectTo ({
              //   url: '../main/main',
              // })
            }
          });
        } else {

        }
      }
    });
  }
})
