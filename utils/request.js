const CONFIG = require("./config.js")

// loading配置，请求次数统计
function startLoading() {
  wx.showLoading({
    title: 'Loading...',
    icon: 'none'
  })
}
function endLoading() {
  wx.hideLoading();
}
// 声明一个对象用于存储请求个数
var needLoadingRequestCount = 0;
function showFullScreenLoading() {
  if (needLoadingRequestCount === 0) {
    startLoading();
  }
  needLoadingRequestCount++;
};
function tryHideFullScreenLoading() {
  if (needLoadingRequestCount <= 0) return;
  needLoadingRequestCount--;
  if (needLoadingRequestCount === 0) {
    endLoading();
  }
};

    const request = (url,method,data) => {

      showFullScreenLoading();

      let _url = CONFIG.API_BASE_URL+url
      return new Promise((resolve ,reject)=>{
        wx.request({
          url: _url,
          method:method,
          data:data,
          header: {
            Authorization: "Vean " + wx.getStorageSync('token')
          },
          success(request) {
            
              if(request.data.code === 0){
                resolve(request.data)
              }else if(request.data.code === 500){
                if (request.data.msg === '运行时异常:com.ak.security.exception.AppSecurityException: Token invalided.') {                    
                    wx.request({
                        url: CONFIG.API_BASE_URL + "/rest/oauth/refreshtoken",
                        method: 'get',
                        data: {
                          openid: wx.getStorageSync('open_id')
                        },
                        success: function (res) {
                          res = res.data;
                          if (res.code == 0) {
        
                            wx.setStorageSync('token', res.data.access_token)
                            wx.setStorageSync('driver_id', res.data.driver_id)
                            wx.setStorageSync('driver_phone', res.data.driver_phone)                            
                            
                          } else {
                            //that.showInfo('res.errmsg');
                          }
                        },
                        fail: function (error) {
                        }
                      });
                }
              }else{
                console.error("服务器返回错误信息"+request.data.msg)
              }
            
          },
          fail(error) {
            reject(error)
          },
          complete(aaa) {
            tryHideFullScreenLoading();
          }
        })
      })
    }
    /**
     * 小程序的promise没有finally方法，自己扩展下
     */
    Promise.prototype.finally = function (callback) {
      var Promise = this.constructor;
      return this.then(
        function (value) {
          Promise.resolve(callback()).then(
            function () {
              return value;
            }
          );
        },
        function (reason) {
         Promise.resolve(callback()).then(
            function () {
              throw reason;
            }
          );
        }
      );
    }
    
    
      //所有接口定义在这里
    module.exports = {
      request,
      //更新GPS
      updategps:(data) => {
        return request('/rest/waybill/updategps','POST',data)
      },
      //获取承运运单
      currenttakewaybill:(data) => {
        return request('/rest/waybill/currenttakewaybill','get')
      }
      
    }